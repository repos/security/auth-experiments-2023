CREATE DATABASE IF NOT EXISTS `wiki_db_1`;
CREATE DATABASE IF NOT EXISTS `wiki_db_1_keycloak`;
CREATE DATABASE IF NOT EXISTS `wiki_db_2`;
CREATE DATABASE IF NOT EXISTS `centralauth`;
CREATE DATABASE IF NOT EXISTS `keycloak`;
GRANT ALL ON `wiki_db_1`.* TO 'root'@'%';
GRANT ALL ON `wiki_db_1_keycloak`.* TO 'root'@'%';
GRANT ALL ON `wiki_db_2`.* TO 'root'@'%';
GRANT ALL ON `centralauth.*` TO 'root'@'%';
GRANT ALL ON `keycloak.*` TO 'root'@'%';
CREATE TABLE `wiki_db_1_keycloak`.`openid_connect` (
  `oidc_user` int(10) unsigned NOT NULL,
  `oidc_subject` tinyblob NOT NULL,
  `oidc_issuer` tinyblob NOT NULL,
  PRIMARY KEY (`oidc_user`),
  KEY `openid_connect_subject` (`oidc_subject`(50),`oidc_issuer`(50))
) ENGINE=InnoDB DEFAULT CHARSET=binary;
