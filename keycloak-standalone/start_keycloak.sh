#!/bin/bash

if [ -z ${CLIENT_SECRET} ]; then
    echo "CLIENT_SECRET env var is not set. Aborting."
    exit 1
fi

/opt/keycloak/bin/kc.sh start --import-realm
