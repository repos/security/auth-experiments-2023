package org.mediawiki.passwordhashingprovider.algorithms;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;
import org.apache.commons.codec.digest.DigestUtils;

public class B implements HashingAlgorithm {
    public String prefix = "B";

    @Override
    public Boolean isValid(String hash, String userPassword) {
        byte[] generatedPassword, passwordFromDB;
        try {
            generatedPassword = generatePassword(userPassword, hash);
            passwordFromDB = getPasswordHash(hash);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }

        return Arrays.equals(generatedPassword, passwordFromDB);
    }

    @Override
    public String getPrefix() {
        return prefix;
    }

    private byte[] generatePassword(String password, String encodedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException {

        return DigestUtils.md5Hex(getSalt(encodedPassword) + "-" + DigestUtils.md5Hex(password)).getBytes();
    }

    private String getSalt(String encodedPassword) {
        String[] hashArray = encodedPassword.split(":");

        return hashArray[2];
    }

    private byte[] getPasswordHash(String encodedPassword) {
        String[] hashArray = encodedPassword.split(":");

        return hashArray[3].getBytes();
    }
}
