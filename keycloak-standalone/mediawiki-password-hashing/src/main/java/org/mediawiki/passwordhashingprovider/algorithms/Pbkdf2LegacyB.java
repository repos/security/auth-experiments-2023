package org.mediawiki.passwordhashingprovider.algorithms;

import org.apache.commons.codec.digest.DigestUtils;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;

public class Pbkdf2LegacyB implements HashingAlgorithm {
    public String prefix = "pbkdf2-legacyB";

    @Override
    public Boolean isValid(String hash, String userPassword) {
        byte[] generatedPassword, passwordFromDB;
        try {
            generatedPassword = generatePassword(userPassword, hash);
            passwordFromDB = getPasswordHash(hash);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }

        return Arrays.equals(generatedPassword, passwordFromDB);
    }

    @Override
    public String getPrefix() {
        return prefix;
    }

    private byte[] generatePassword(String password, String encodedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException {
        int iterations = getIterations(encodedPassword);
        int length = getLength(encodedPassword) * 8;
        char[] chars = DigestUtils.md5Hex(getBSalt(encodedPassword) + "-" + DigestUtils.md5Hex(password)).toCharArray();
        byte[] salt = getSalt(encodedPassword);
        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, length);
        String algorithm = getAlgorithm(encodedPassword);
        SecretKeyFactory skf;
        if (algorithm.equals(HashingAlgorithm.SHA512)) {
            skf = SecretKeyFactory.getInstance(HashingAlgorithm.PBKDF2_SHA512);
        } else {
            skf = SecretKeyFactory.getInstance(HashingAlgorithm.PBKDF2_SHA256);
        }

        return skf.generateSecret(spec).getEncoded();
    }

    private String getAlgorithm(String encodedPassword) {
        String[] hashArray = encodedPassword.split("!")[1].split(":");

        return hashArray[0];
    }

    private int getIterations(String encodedPassword) {
        String[] hashArray = encodedPassword.split("!")[1].split(":");

        return Integer.parseInt(hashArray[1]);
    }

    private int getLength(String encodedPassword) {
        String[] hashArray = encodedPassword.split("!")[1].split(":");

        return Integer.parseInt(hashArray[2]);
    }

    private String getBSalt(String encodedPassword) {
        String[] hashArray = encodedPassword.split("!");

        return hashArray[2];
    }

    private byte[] getSalt(String encodedPassword) {
        String[] hashArray = encodedPassword.split("!");

        return Base64.getDecoder().decode(hashArray[3].getBytes());
    }

    private byte[] getPasswordHash(String encodedPassword) {
        String[] hashArray = encodedPassword.split("!");

        return Base64.getDecoder().decode(hashArray[4].getBytes());
    }
}
