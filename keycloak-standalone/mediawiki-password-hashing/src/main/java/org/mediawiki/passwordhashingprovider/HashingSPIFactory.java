package org.mediawiki.passwordhashingprovider;

import org.keycloak.Config;
import org.keycloak.credential.hash.PasswordHashProvider;
import org.keycloak.credential.hash.PasswordHashProviderFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

public class HashingSPIFactory implements PasswordHashProviderFactory {
    public static final String PROVIDER_NAME = "mediawiki-password-hashing";

    @Override
    public PasswordHashProvider create(KeycloakSession keycloakSession) {
        return new HashingSPI();
    }

    @Override
    public void init(Config.Scope scope) {

    }

    @Override
    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {

    }

    @Override
    public void close() {

    }

    @Override
    public String getId() {
        return PROVIDER_NAME;
    }
}
