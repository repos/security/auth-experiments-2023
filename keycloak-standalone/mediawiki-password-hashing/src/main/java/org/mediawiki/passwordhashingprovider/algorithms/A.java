package org.mediawiki.passwordhashingprovider.algorithms;

import org.apache.commons.codec.digest.DigestUtils;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;

public class A implements HashingAlgorithm {
    public String prefix = "A";

    @Override
    public Boolean isValid(String hash, String userPassword) {
        byte[] generatedPassword, passwordFromDB;
        try {
            generatedPassword = generatePassword(userPassword, hash);
            passwordFromDB = getPasswordHash(hash);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }

        return Arrays.equals(generatedPassword, passwordFromDB);
    }

    @Override
    public String getPrefix() {
        return prefix;
    }

    private byte[] generatePassword(String password, String encodedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String[] hashArray = encodedPassword.split(":");
        if (Arrays.stream(hashArray).count() == 4) {
            return DigestUtils.md5Hex(getSalt(encodedPassword) + "-" + DigestUtils.md5Hex(password)).getBytes();
        } else {
            return DigestUtils.md5Hex(password).getBytes();
        }
    }

    private String getSalt(String encodedPassword) {
        String[] hashArray = encodedPassword.split(":");

        return hashArray[2];
    }

    private byte[] getPasswordHash(String encodedPassword) {
        String[] hashArray = encodedPassword.split(":");
        if (Arrays.stream(hashArray).count() == 4) {
            return hashArray[3].getBytes();
        } else {
            return hashArray[2].getBytes();
        }
    }
}
