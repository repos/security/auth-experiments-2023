package org.mediawiki.passwordhashingprovider.algorithms;

public interface HashingAlgorithm {

    public static final String PBKDF2_SHA512 = "PBKDF2WithHmacSHA512";
    public static final String PBKDF2_SHA256 = "PBKDF2WithHmacSHA256";
    public static final String SHA512 = "sha512";
    public static final String SHA256 = "sha256";

    Boolean isValid(String hash, String userPassword);

    String getPrefix();
}
