package org.mediawiki.passwordhashingprovider;

import org.keycloak.credential.hash.PasswordHashProvider;
import org.keycloak.models.PasswordPolicy;
import org.keycloak.models.credential.PasswordCredentialModel;
import org.mediawiki.passwordhashingprovider.algorithms.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class HashingSPI implements PasswordHashProvider{
    public List<HashingAlgorithm> algorithms = new ArrayList<>();

    public HashingSPI() {
        algorithms.add(new Pbkdf2());
        algorithms.add(new Pbkdf2LegacyB());
        algorithms.add(new Pbkdf2LegacyA());
        algorithms.add(new B());
        algorithms.add(new A());
    }

    public Boolean isValid(String hash, String userPassword) {
        AtomicReference <HashingAlgorithm> currentAlgorithm = new AtomicReference<>();

        algorithms.forEach((algorithm) -> {
            if (hash.startsWith(algorithm.getPrefix() + ":", 1)) {
                currentAlgorithm.set(algorithm);
            }
        });

        if (currentAlgorithm.get() == null) {
            return false;
        }

        return currentAlgorithm.get().isValid(hash, userPassword);
    }

    /**
     * Get an Algorithm class instance.
     * Potentially for future purposes.
     * 
     * @param hash String
     * @param userPassword String
     * @return HashingAlgorithm
     */
    public HashingAlgorithm getAlgorithm(String hash, String userPassword) {
        for (HashingAlgorithm algorithm : algorithms) {
            if (hash.startsWith(algorithm.getPrefix() + ":", 1)) {
                return algorithm;
            }
        }

        return null;
    }

    @Override
    public boolean policyCheck(PasswordPolicy passwordPolicy, PasswordCredentialModel passwordCredentialModel) {
        return true;
    }

    @Override
    public PasswordCredentialModel encodedCredential(String s, int i) {
        return null;
    }

    @Override
    public boolean verify(String s, PasswordCredentialModel passwordCredentialModel) {
        return this.isValid(passwordCredentialModel.getPasswordCredentialData().toString(), s);
    }

    @Override
    public void close() {

    }
}
