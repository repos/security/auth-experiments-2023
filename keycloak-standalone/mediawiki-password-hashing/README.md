# Mediawiki Password Hashing Provider Documentation
This is a Keycloak password hashing provider for MediaWiki platform. With this provider MediaWiki users' passwords can be hashed same way as MediaWiki does. So users can login into Keycloak without reseting a password.

## Provider setup
Provider setup described in [User Federation Provider Documentation](https://gitlab.gluzdov.com/auth-experiment/mediawiki-common/-/tree/main/keycloak-standalone/mediawiki-user-federation)

## Codebase description

### Classes and Methods description

#### Class HashingSPIFactory
Used to initialize provider in Keycloak system.

#### Class HashingSPI
The main class of provider. Used to initialize possible algorithms and check if the provided password is valid after choosing the appropriate algorithm.
- verify() - default Keycloak method that automatically evaluated during login process. It will run isValid() function.
- isValid() - gets all available algorithms, choose the algorithm according to MediaWiki password hash, and runs isValid() inside the algorithm class. Can be run separately from verify().

#### Algorithm Class
Have different isValid() implementations accroding to MediaWiki password hashing algorithm.
