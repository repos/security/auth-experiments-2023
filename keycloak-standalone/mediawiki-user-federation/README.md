# Mediawiki User Federation Provider Documentation
This is a Keycloak federation provider for Mediawiki platform. With this provider users from mediawiki DB can act like users from Keycloak without physical user migration from Mediawiki DB into Keycloak's one.

## Provider setup

1. Download [Keycloak-18.0.1](https://github.com/keycloak/keycloak/releases/download/18.0.1/keycloak-18.0.1.tar.gz) to your local machine or latest version from [Official git](https://github.com/keycloak/keycloak/releases).
2. Unpack it to your project's dir.
3. Clone [this](https://gitlab.gluzdov.com/auth-experiment/mediawiki-common.git) repo to your projects dir.
4. Go into `keycloak-standalone/mediawiki-password-hashing` folder.
5. Pack Password Hashing SPI into jar with `mvn clean package` command.
6. Go into `keycloak-standalone/mediawiki-user-federation`.
7. Change dependency in `<systemPath>/mph/dist/mediawiki-password-hashing.jar</systemPath>` section of `pom.xml` file to let Federation provider know where is Password Hashing jar located. You should use absolute path. So it will be something like: In our case there will be `/Users/{localUsername}/projects/mediawiki-common/keycloak-standalone/mediawiki-password-hashing/dist/mediawiki-password-hashing.jar`.
8. Pack this provider into jar with `mvn clean package` command. Jar file will be placed under the `dist` folder.
9. Copy this jar to `keycloak/providers/` dir.
10. Go to Keycloak folder and run `bin/kc.sh build` to build Keycloak with our provider.
11. Run `bin/kc.sh start` to start project or `bin/kc.sh --verbose --debug start-dev` to run in debug and verbose mode.
12. In the end it will show you host and port where it is started. Usually http://0.0.0.0:8080. Open this host. After the first run, you will have to create admin account. Then you can click on `Admin console`. To have quick access to Admin console you can add to host **/admin/master/console**, so it will be http://0.0.0.0:8080/admin/master/console.
13. Login into admin console with provided url.
14. Create a new realm or use one of the already existing. It is not recommended to use the Master realm, because it is used for Keycloak administration.

    ![New realm](assets/images/new_realm.png)
15. Use your left menu, navigate to User Federation, and choose `mediawiki-user-provider` provider.
    
    ![Choose provider](assets/images/choose_rovider.png)
16. You should feel in these fields (eg.):
    - MySQL URI: `jdbc:mysql://localhost/my_wiki`
    It is used to connect to DB. URI fields meaning: `jdbc:vendor://hostname:port/db_name`.
    - MySQL DB Username and MySQL DB Password to get DB access.
    - Users table: user tells in what table users are stored.
    - Username, user real name, email and password columns to perform queries and get
    minimum needed info for Keycloak from Mediawiki DB.

    ![Provider configuration](assets/images/provider_config.png)
######  17. Also, there is a possibility to configure and run User sync process.
18. Set two fields: `Batch Size` and `Last synced User id`. Set `Last synced User id` to 0 if it is first run or some user id if you want to start sync from next id. It will be automatically increased during sync process.  `Batch Size` is amount of users that will be imported in one batch, for example you can set 1000.

    ![Sync settings](assets/images/sync_settings.png)
19. Configure periods for Full and Partial sync if needed. This period is amount of second after which sync will be repeated.

    ![Sync settings](assets/images/periodic_sync_settings.png)
20. Disable cache (Cache Policy: NO_CACHE).
21. Save your configuration.
22. Click on `Synchronize all users` to start sync.
23. Navigate to the Users section and search for some users or click ‘View all users’.

    ![User search](assets/images/user_search.png)
24. Clear cache before new sync in **Realm Settings** -> **Cache** -> **Realm Cache** -> **Clear**

## Codebase description

### Classes Diagram
![Provider classes flow diagram](assets/images/process_flow_diagram.png)

### Classes and Methods description

#### Class DBUserStorageProviderFactory
It is used to add fields for UI that you will see in Federation Provider configuration section. It checks DB if all needed fields are set and also checks DB connection.
- create() - creates instance of DBUserStorageProvider class.
- getId() - returns provider id that will be shown on add provider page.
- getConfigProperties() - returns configuration options (future UI fields) that stored in static configMetadata property.
- validateConfiguration() - your custom config check rules.

#### Class DBUserStorageProvider
Main provider's class. Used to search and get user by id/mail/username, validate password, etc.
- supportsCredentialType() - checks if credential type is one of supported.
- isConfiguredFor() - checks what credential types are configured (password, OTP, recovery code, WebAuthn).
- isValid() - checks if password is valid.
- getUserById() - get user by id. Used when you opening user profile in admin console.
- getUserByUsername() - get user by username. Mostly used during login.
- getUserByEmail() - get user by email. Mostly used during login.
- findUser() - find user by different params using DataSource class. initiated by previous getUser... functions.
- getUsersCount() - checks amount of users to build pagination.
- getUsersStream() - should be ued to get all users.
- searchForUser() - with different params used to search user by param or all users.
- toUserModelStream() - converts founded users into UserModel by using Adapter class.

#### Class DataSource
Responsible for DB connection and querying users.
- getConnection() - to get DB connection.
- getUsersCount() - makes user count query.
- getUsers() - get all users from DB.
- getUser() - get user by search string (username, email, real name).
- getUserById() - fetch user from DB by id field.

#### Class Mediawiki
This is data object that describes Mediawiki user. Used in DataSource class.
- All getters and setters implemented by Lombok library.

#### Class UserAdapter
Keycloak UserAdapter class. Describes Keycloak user with all needed methods as UserModel and uses Mediawiki class to get user data. 
- getUsername(); getFirstName(); getLastName(); getEmail() - getters to pass data from Provider to Keycloak.


### DB connection (DataSource class) fine-tuning
###### TODO

## Building Keycloak with new Provider

### For local dev or standalone server

1. Download [Keycloak-18.0.1](https://github.com/keycloak/keycloak/releases/download/18.0.1/keycloak-18.0.1.tar.gz) to your local machine or latest version from [Official git](https://github.com/keycloak/keycloak/releases).
2. Unpack it to your project's dir.
3. Put your previously built jar with Provider into the ‘providers’ folder (keycloak-{version}/providers)
4. Build Keycloak with the new provider using the command: `kc.sh --verbose build` (--verbose to see detailed output)
5. Start Keycloak with the command: bin/kc.sh [options] start-dev (use options --verbose --debug to see the detailed output and start to debug mode).


### For cloud + CICD

You can use this Dockerfile like an example of how to deploy Keycloak and User federation provider into dockerized environment: [Dockerfile](https://gitlab.gluzdov.com/auth-experiment/mediawiki-common/-/blob/main/keycloak-standalone/Dockerfile)

## Tests
###### TODO
