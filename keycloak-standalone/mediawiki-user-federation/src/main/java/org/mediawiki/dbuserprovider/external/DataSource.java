package org.mediawiki.dbuserprovider.external;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.jboss.logging.Logger;
import org.keycloak.component.ComponentModel;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class DataSource implements Closeable {
    private final ComponentModel model;
    private final ExecutorService executor = Executors.newFixedThreadPool(1);
    private static HikariDataSource ds;
    private static final HikariConfig config = new HikariConfig();
    private static final Logger logger = Logger.getLogger(DataSource.class);

    public DataSource(ComponentModel model) {
        this.model = model;

        config.setJdbcUrl(model.getConfig().getFirst("mysql"));
        config.setUsername(model.getConfig().getFirst("db_username"));
        config.setPassword(model.getConfig().getFirst("db_password"));
        config.setDriverClassName(com.mysql.cj.jdbc.Driver.class.getName());
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        config.setIdleTimeout(10000);
        config.setMaxLifetime(30000);
        config.setMaximumPoolSize(20);

        HikariDataSource newDS = new HikariDataSource(config);
        HikariDataSource old = ds;
        ds = newDS;
        disposeOldDataSource(old);
    }

    private void disposeOldDataSource(HikariDataSource old) {
        executor.submit(() -> {
            try {
                if (old != null) {
                    old.close();
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        });
    }

    public void close() {
        executor.shutdownNow();
        try {
            executor.awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        logger.infov("Executor shot down");
        if (ds != null && executor.isTerminated()) {
            ds.close();
            logger.infov("DataSource closed");
        }
    }

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    public Integer getUsersCount() throws SQLException {
        PreparedStatement pstmt;
        ResultSet rs;
        Connection conn;
        String query = "SELECT COUNT(" + this.model.getConfig().getFirst("usernamecol") + ") FROM "
                + this.model.getConfig().getFirst("table") + ";";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(query);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        rs = pstmt.executeQuery();
        ds.evictConnection(conn);

        return Integer.valueOf(rs.toString());
    }

    public List<MediawikiUser> getUsers() {
        PreparedStatement pstmt;
        ResultSet rs;
        Connection conn;

        try {
            String query = "SELECT " + this.model.getConfig().getFirst("usernamecol") + ", "
                    + this.model.getConfig().getFirst("realnamecol") + ", "
                    + this.model.getConfig().getFirst("emailcol") + ", "
                    + this.model.getConfig().getFirst("passwordcol") + " FROM "
                    + this.model.getConfig().getFirst("table");
            conn = getConnection();
            pstmt = conn.prepareStatement(query);
            rs = pstmt.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        List<MediawikiUser> users = new ArrayList<>();
        MediawikiUser user;
        try {
            while(rs.next()) {
                user = new MediawikiUser();
                String firstName = "";
                String lastName = "";
                String realName = rs.getString(this.model.getConfig().getFirst("realnamecol"));
                if (!realName.contains(" ")) {
                    firstName = realName;
                } if (realName.contains(" ")){
                    firstName = realName.substring(0, realName.indexOf(" "));
                    lastName = realName.substring(realName.indexOf(" ")+1);
                }
                user.setUsername(rs.getString(this.model.getConfig().getFirst("usernamecol")));
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(rs.getString(this.model.getConfig().getFirst("emailcol")));
                user.setPassword(rs.getString(this.model.getConfig().getFirst("passwordcol")));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        ds.evictConnection(conn);

        return users;
    }

    public List<MediawikiUser> getUser(String search) {
        PreparedStatement pstmt;
        ResultSet rs;
        Connection conn;
        List<MediawikiUser> users = new ArrayList<>();
        MediawikiUser user;

        try {
            String query = "SELECT " + this.model.getConfig().getFirst("usernamecol") + ", "
                    + this.model.getConfig().getFirst("realnamecol") + ", "
                    + this.model.getConfig().getFirst("emailcol") + ", "
                    + this.model.getConfig().getFirst("passwordcol") + " FROM "
                    + this.model.getConfig().getFirst("table") + " WHERE "
                    + this.model.getConfig().getFirst("usernamecol") + " like ? OR "
                    + this.model.getConfig().getFirst("emailcol") + " like ? OR "
                    + this.model.getConfig().getFirst("realnamecol") + " like ?;";
            conn = getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, "%" + search + "%");
            pstmt.setString(2, "%" + search + "%");
            pstmt.setString(3, "%" + search + "%");
            rs = pstmt.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            while(rs.next()) {
                user = new MediawikiUser();
                String firstName = "";
                String lastName = "";
                String realName = rs.getString(this.model.getConfig().getFirst("realnamecol"));
                if (!realName.contains(" ")) {
                    firstName = realName;
                } if (realName.contains(" ")){
                    firstName = realName.substring(0, realName.indexOf(" "));
                    lastName = realName.substring(realName.indexOf(" ")+1);
                }
                user.setUsername(rs.getString(this.model.getConfig().getFirst("usernamecol")));
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(rs.getString(this.model.getConfig().getFirst("emailcol")));
                user.setPassword(rs.getString(this.model.getConfig().getFirst("passwordcol")));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        ds.evictConnection(conn);

        return users;
    }

    public MediawikiUser getUserById(String search) {
        PreparedStatement pstmt;
        ResultSet rs;
        Connection conn;
        MediawikiUser user = null;

        try {
            String query = "SELECT " + this.model.getConfig().getFirst("usernamecol") + ", "
                    + this.model.getConfig().getFirst("realnamecol") + ", "
                    + this.model.getConfig().getFirst("emailcol") + ", "
                    + this.model.getConfig().getFirst("passwordcol") + " FROM "
                    + this.model.getConfig().getFirst("table") + " WHERE "
                    + this.model.getConfig().getFirst("usernamecol") + " = ?;";
            conn = getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, search);
            rs = pstmt.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            while(rs.next()) {
                user = new MediawikiUser();
                String firstName = "";
                String lastName = "";
                String realName = rs.getString(this.model.getConfig().getFirst("realnamecol"));
                if (!realName.contains(" ")) {
                    firstName = realName;
                } if (realName.contains(" ")){
                    firstName = realName.substring(0, realName.indexOf(" "));
                    lastName = realName.substring(realName.indexOf(" "));
                }
                user.setUsername(rs.getString(this.model.getConfig().getFirst("usernamecol")));
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(rs.getString(this.model.getConfig().getFirst("emailcol")));
                user.setPassword(rs.getString(this.model.getConfig().getFirst("passwordcol")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        ds.evictConnection(conn);

        return user;
    }

    public MediawikiUser getUserByEmail(String search) {
        PreparedStatement pstmt;
        ResultSet rs;
        Connection conn;
        MediawikiUser user = null;

        try {
            String query = "SELECT " + this.model.getConfig().getFirst("usernamecol") + ", "
                    + this.model.getConfig().getFirst("realnamecol") + ", "
                    + this.model.getConfig().getFirst("emailcol") + ", "
                    + this.model.getConfig().getFirst("passwordcol") + " FROM "
                    + this.model.getConfig().getFirst("table") + " WHERE "
                    + this.model.getConfig().getFirst("emailcol") + " = ?;";
            conn = getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, search);
            rs = pstmt.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            while(rs.next()) {
                user = new MediawikiUser();
                String firstName = "";
                String lastName = "";
                String realName = rs.getString(this.model.getConfig().getFirst("realnamecol"));
                if (!realName.contains(" ")) {
                    firstName = realName;
                } if (realName.contains(" ")){
                    firstName = realName.substring(0, realName.indexOf(" "));
                    lastName = realName.substring(realName.indexOf(" "));
                }
                user.setUsername(rs.getString(this.model.getConfig().getFirst("usernamecol")));
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(rs.getString(this.model.getConfig().getFirst("emailcol")));
                user.setPassword(rs.getString(this.model.getConfig().getFirst("passwordcol")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        ds.evictConnection(conn);

        return user;
    }

    public List<MediawikiUser> getUsersBunch(String start, String limit) {
        PreparedStatement pstmt;
        ResultSet rs;
        Connection conn;
        List<MediawikiUser> users = new ArrayList<>();
        MediawikiUser user;

        try {
            String query = "SELECT " + this.model.getConfig().getFirst("usernamecol") + ", "
                    + "gu_id, "
                    + this.model.getConfig().getFirst("realnamecol") + ", "
                    + this.model.getConfig().getFirst("emailcol") + ", "
                    + this.model.getConfig().getFirst("passwordcol") + " FROM "
                    + this.model.getConfig().getFirst("table") + " WHERE "
                    + "gu_id > " + start + " "
                    + "LIMIT " + limit + ";";
            conn = getConnection();
            pstmt = conn.prepareStatement(query);
            rs = pstmt.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            while(rs.next()) {
                user = new MediawikiUser();
                String firstName = "";
                String lastName = "";
                String realName = rs.getString(this.model.getConfig().getFirst("realnamecol"));
                if (!realName.contains(" ")) {
                    firstName = realName;
                } if (realName.contains(" ")){
                    firstName = realName.substring(0, realName.indexOf(" "));
                    lastName = realName.substring(realName.indexOf(" ")+1);
                }
                user.setUserId(rs.getString("gu_id"));
                user.setUsername(rs.getString(this.model.getConfig().getFirst("usernamecol")));
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(rs.getString(this.model.getConfig().getFirst("emailcol")));
                user.setPassword(rs.getString(this.model.getConfig().getFirst("passwordcol")));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        ds.evictConnection(conn);

        return users;
    }
}
