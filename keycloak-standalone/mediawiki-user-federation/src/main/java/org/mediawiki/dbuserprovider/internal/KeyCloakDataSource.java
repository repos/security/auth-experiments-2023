package org.mediawiki.dbuserprovider.internal;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.jboss.logging.Logger;
import org.keycloak.component.ComponentModel;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class KeyCloakDataSource implements Closeable {
    private final ExecutorService executor = Executors.newFixedThreadPool(1);
    private static HikariDataSource ds;
    private static final HikariConfig config = new HikariConfig();
    private static final Logger logger = Logger.getLogger(KeyCloakDataSource.class);

    public KeyCloakDataSource(ComponentModel model) {

        /* Get values from ENV variables */
        config.setJdbcUrl(model.getConfig().getFirst("keycloak_mysql"));
        config.setUsername(model.getConfig().getFirst("keycloak_db_username"));
        config.setPassword(model.getConfig().getFirst("keycloak_db_password"));
        config.setDriverClassName(com.mysql.cj.jdbc.Driver.class.getName());
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        config.setIdleTimeout(10000);
        config.setMaxLifetime(30000);
        config.setMaximumPoolSize(20);

        HikariDataSource newDS = new HikariDataSource(config);
        HikariDataSource old = ds;
        ds = newDS;
        disposeOldDataSource(old);
    }

    private void disposeOldDataSource(HikariDataSource old) {
        executor.submit(() -> {
            try {
                if (old != null) {
                    old.close();
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        });
    }

    public void close() {
        executor.shutdownNow();
        try {
            executor.awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        logger.infov("Executor shot down");
        if (ds != null && executor.isTerminated()) {
            ds.close();
            logger.infov("DataSource closed");
        }
    }

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    public void setLastSyncId(String componentId, String lastSyncId) {
        //TODO: change the way we store last sync id
        PreparedStatement pstmt;
        Connection conn;

        try {
            String query = "UPDATE " + "COMPONENT_CONFIG " + "SET "
                    + "VALUE = " + "?"
                    + " WHERE "
                    + "COMPONENT_ID = " + "? "
                    + "AND NAME = " + "'lastSyncId'";
            conn = getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, lastSyncId);
            pstmt.setString(2, componentId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        ds.evictConnection(conn);
    }
}
