package org.mediawiki.dbuserprovider;

import org.jboss.logging.Logger;
import org.json.JSONObject;
import org.keycloak.common.util.Time;
import org.keycloak.component.ComponentModel;
import org.keycloak.credential.*;
import org.keycloak.models.*;
import org.keycloak.models.credential.PasswordCredentialModel;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.user.UserLookupProvider;
import org.keycloak.storage.user.UserQueryProvider;
import org.keycloak.storage.user.UserRegistrationProvider;
import org.mediawiki.dbuserprovider.external.DataSource;
import org.mediawiki.dbuserprovider.external.MediawikiUser;
import org.mediawiki.dbuserprovider.internal.KeyCloakDataSource;
import org.mediawiki.passwordhashingprovider.HashingSPI;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DBUserStorageProvider implements UserStorageProvider,
        UserLookupProvider, UserQueryProvider,
        UserRegistrationProvider, CredentialInputValidator,
        CredentialInputUpdater {
    private final KeyCloakDataSource kcds;
    protected KeycloakSession session;
    protected ComponentModel model;
    private final DataSource dataSource;
    private final HashingSPI hashingSPI;

    private static final Logger logger = Logger.getLogger(DBUserStorageProvider.class);

    public DBUserStorageProvider(KeycloakSession session, ComponentModel model, DataSource dataSource, HashingSPI hashingSPI) {

        this.session = session;
        this.model = model;
        this.dataSource = dataSource;
        this.hashingSPI = hashingSPI;
        this.kcds = new KeyCloakDataSource(model);
    }

    @Override
    public void close() {
        logger.infov("Closing provider");
        dataSource.close();
        logger.infov("DataSource closed");
    }

    @Override
    public void preRemove(RealmModel realm) {
        UserStorageProvider.super.preRemove(realm);
    }

    @Override
    public void preRemove(RealmModel realm, GroupModel group) {
        UserStorageProvider.super.preRemove(realm, group);
    }

    @Override
    public void preRemove(RealmModel realm, RoleModel role) {
        UserStorageProvider.super.preRemove(realm, role);
    }

    @Override
    public UserModel getUserById(RealmModel realm, String id) {
        return UserLookupProvider.super.getUserById(realm, id);
    }

    @Override
    public UserModel getUserById(String s, RealmModel realm) {
        // used when we open user in admin panel
        logger.infov("Get user by id: userId={0}", s);
        String externalId = StorageId.externalId(s);
        MediawikiUser user = dataSource.getUserById(externalId);
        if (user == null) return null;

        return new UserAdapter(session, realm, model, user);
    }

    @Override
    public UserModel getUserByUsername(RealmModel realm, String username) {
        logger.infov("Get user #1 by username: userName={0}", username);
        return UserLookupProvider.super.getUserByUsername(realm, username);
    }

    @Override
    public UserModel getUserByUsername(String username, RealmModel realm) {
        if (this.session.getContext().getAuthenticationSession() != null) {
            username = this.session.getContext().getAuthenticationSession().getAuthNote("ATTEMPTED_USERNAME");
        }
        logger.infov("Get user #2 by username: userName={0}", username);
        MediawikiUser user = dataSource.getUserById(username);
        if (user == null) return null;

        return new UserAdapter(session, realm, model, user);
    }

    @Override
    public UserModel getUserByEmail(RealmModel realm, String email) {
        return UserLookupProvider.super.getUserByEmail(realm, email);
    }

    @Override
    public UserModel getUserByEmail(String s, RealmModel realm) {
        logger.infov("Get user by email");
        MediawikiUser user = dataSource.getUserByEmail(s);
        if (user == null) return null;

        return new UserAdapter(session, realm, model, user);
    }

    @Override
    public UserModel addUser(RealmModel realm, String s) {
        logger.infov("Add user to the federated system: userName={0}", s);
        return null;
    }

    @Override
    public boolean removeUser(RealmModel realm, UserModel userModel) {
        logger.infov("Remove user from federated system");
        return false;
    }

    @Override
    public boolean updateCredential(RealmModel realm, UserModel userModel, CredentialInput credentialInput) {
        logger.infov("Update user called");
        return false;
    }

    @Override
    public void disableCredentialType(RealmModel realm, UserModel userModel, String s) {

    }

    @Override
    public Set<String> getDisableableCredentialTypes(RealmModel realm, UserModel userModel) {
        return null;
    }

    @Override
    public boolean supportsCredentialType(String credentialType) {
        return credentialType.equals(PasswordCredentialModel.TYPE);
    }

    @Override
    public boolean isConfiguredFor(RealmModel realm, UserModel user, String credentialType) {
        return PasswordCredentialModel.TYPE.equals(credentialType);
    }

    @Override
    public boolean isValid(RealmModel realm, UserModel userModel, CredentialInput credentialInput) {
        String encodedPassword = ((UserAdapter) userModel).getPassword();

        return hashingSPI.isValid(encodedPassword, credentialInput.getChallengeResponse());
    }

    @Override
    public int getUsersCount(RealmModel realm) {
        try {
            return this.dataSource.getUsersCount();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Stream<UserModel> getUsersStream(RealmModel realm, Integer firstResult, Integer maxResults) {
        return UserQueryProvider.super.getUsersStream(realm, firstResult, maxResults);
    }

    @Override
    public List<UserModel> getUsers(RealmModel realm) {
        logger.infov("Get all users");
        return null;
    }

    @Override
    public List<UserModel> getUsers(RealmModel realm, int i, int i1) {
        logger.infov("Get all users with i and i1 params");
        return null;
    }

    @Override
    public List<UserModel> searchForUser(String s, RealmModel realm) {
        logger.infov("Checking user: userId={0}", s);
        return null;
    }

    @Override
    public List<UserModel> searchForUser(String username, RealmModel realm, int i, int i1) {
        logger.infov("Search for user: userName={0}", username);
        List<MediawikiUser> users = this.dataSource.getUser(username);
        return toUserModelStream(users, realm);
    }

    @Override
    public List<UserModel> searchForUser(Map<String, String> map, RealmModel realm) {
        logger.infov("Search for user 1 called");
        return null;
    }

    @Override
    public List<UserModel> searchForUser(Map<String, String> map, RealmModel realm, int i, int i1) {
        logger.infov("Search for user 2 called");
        List<MediawikiUser> users = this.dataSource.getUsers();
        return toUserModelStream(users, realm);
    }

    private List<UserModel> toUserModelStream(List<MediawikiUser> users, RealmModel realm) {
        logger.infov("Received {0} users from provider", users.size());
        return users.stream().map(user -> new UserAdapter(session, realm, model, user)).collect(Collectors.toList());
    }

    @Override
    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel groupModel) {
        return null;
    }

    @Override
    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel groupModel, int i, int i1) {
        return null;
    }

    @Override
    public List<UserModel> searchForUserByUserAttribute(String s, String s1, RealmModel realm) {
        logger.infov("Search for user by attribute called");
        return null;
    }

    public UserModel importUserFromMediawiki(KeycloakSession session, RealmModel realm, MediawikiUser mediawikiUser) {
        /* Searching user by username if there is no such user continue with import */
        UserModel imported = session.userLocalStorage().getUserByUsername(realm, mediawikiUser.getUsername());
        if (imported == null) {
            imported = session.userLocalStorage().addUser(realm, mediawikiUser.getUsername());
            imported.setFirstName(mediawikiUser.getFirstName());
            imported.setLastName(mediawikiUser.getLastName());
            imported.setEmail(mediawikiUser.getEmail());
            imported.setEnabled(true);

            /* Splitting password field. Checking if it there is a password and algorithm is pbkdf2 */
            String[] passwordHashArray = mediawikiUser.getPassword().split(":");
            if (passwordHashArray.length > 1 && Objects.equals(passwordHashArray[1], "pbkdf2")) {

                /* Recombining Mediawiki password hash into Keycloak password hash format */
                String credentialData = prepareCredentialData(passwordHashArray);
                String secretData = prepareSecretData(passwordHashArray);

                /* Creating and setting credential */
                CredentialModel credential = new CredentialModel();
                credential.setType(CredentialRepresentation.PASSWORD);
                credential.setCredentialData(credentialData);
                credential.setSecretData(secretData);
                credential.setCreatedDate(Time.currentTimeMillis());
                session.userCredentialManager().createCredential(realm, imported, credential);
            } else {
                /* If there is no password ask user to reset password */
                imported.setEnabled(false);
                imported.addRequiredAction(UserModel.RequiredAction.UPDATE_PASSWORD);
                logger.infov("User with Username: {0} have no password", imported.getUsername());
            }
            /* Update last synced id to know starting point if sync was interrupted */
            kcds.setLastSyncId(model.getId(), mediawikiUser.getUserId());

            logger.infov("Imported new user from Mediawiki to Keycloak DB. Username: {0}, Email: {1}, Mediawiki_ID: {2}",
                    imported.getUsername(), imported.getEmail(), mediawikiUser.getUserId());
        } else {
            logger.infov("User with Username: {0} already exists", imported.getUsername());
            /* Set last sync id to avoid loop situation if user already exist */
            kcds.setLastSyncId(model.getId(), mediawikiUser.getUserId());
        }

        return imported;
    }

    private String prepareCredentialData(String[] passwordHashArray) {
//        String credentialData;
        JSONObject credentialData = new JSONObject();
        credentialData.put("hashIterations", passwordHashArray[3]);
        credentialData.put("algorithm", "pbkdf2-"+passwordHashArray[2]);
        credentialData.put("additionalParameters", new JSONObject());

        return credentialData.toString();
    }

    private String prepareSecretData(String[] passwordHashArray) {
        JSONObject secretData = new JSONObject();
        secretData.put("value", passwordHashArray[6]);
        secretData.put("salt", passwordHashArray[5]);
        secretData.put("additionalParameters", new JSONObject());

        return secretData.toString();
    }
}
