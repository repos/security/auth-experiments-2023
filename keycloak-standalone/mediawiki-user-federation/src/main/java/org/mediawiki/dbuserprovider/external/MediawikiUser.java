package org.mediawiki.dbuserprovider.external;

import lombok.Data;
import java.util.List;

@Data
public class MediawikiUser {
    private String userId;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private List<String> groups;
    private List<String> roles;
}
