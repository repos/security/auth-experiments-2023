package org.mediawiki.dbuserprovider;

import org.jboss.logging.Logger;
import org.keycloak.component.ComponentModel;
import org.keycloak.component.ComponentValidationException;
import org.keycloak.models.*;
import org.keycloak.models.utils.KeycloakModelUtils;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.provider.ProviderConfigurationBuilder;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.UserStorageProviderFactory;
import org.keycloak.storage.UserStorageProviderModel;
import org.keycloak.storage.user.ImportSynchronization;
import org.keycloak.storage.user.SynchronizationResult;
import org.keycloak.utils.StringUtil;
import org.mediawiki.dbuserprovider.external.DataSource;
import org.mediawiki.dbuserprovider.external.MediawikiUser;
import org.mediawiki.passwordhashingprovider.HashingSPI;

import java.sql.SQLException;
import java.util.*;

public class DBUserStorageProviderFactory implements UserStorageProviderFactory<DBUserStorageProvider>, ImportSynchronization {

    protected static final List<ProviderConfigProperty> configMetadata;
    public static final String PROVIDER_NAME = "mediawiki-user-provider";
    private static final Logger logger = Logger.getLogger(DBUserStorageProviderFactory.class);

    @Override
    public DBUserStorageProvider create(KeycloakSession keycloakSession, ComponentModel componentModel) {
        DataSource dataSource;
        HashingSPI hashingSPI = new HashingSPI();
        try {
            dataSource = new DataSource(componentModel);
            dataSource.getConnection();
        } catch (SQLException e) {
            // handle any errors
            logger.error("SQLException: " + e.getMessage());
            logger.error("SQLState: " + e.getSQLState());
            logger.error("VendorError: " + e.getErrorCode());
            throw new ComponentValidationException(e.getMessage());
        }

        return new DBUserStorageProvider(keycloakSession, componentModel, dataSource, hashingSPI);
    }

    @Override
    public String getId() {
        return PROVIDER_NAME;
    }

    static {
        configMetadata = ProviderConfigurationBuilder.create()
                .property().name("mysql")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("jdbc:mysql://localhost/my_wiki_test")
                .label("MySQL URI")
                .helpText("MySQL URI to connect to DB").add()

                .property().name("db_username")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("admin")
                .label("MySQL DB Username")
                .helpText("MySQL username for DB connection").add()

                .property().name("db_password")
                .type(ProviderConfigProperty.PASSWORD)
                .defaultValue("password")
                .label("MySQL DB Password")
                .helpText("MySQL password for DB connection").add()

                .property().name("table")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("user")
                .label("Users Table")
                .helpText("Table where users are stored").add()

                .property().name("usernamecol")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("user_name")
                .label("Username Column")
                .helpText("Column name that holds the usernames").add()

                .property().name("realnamecol")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("user_real_name")
                .label("User real name Column")
                .helpText("Column name that holds the real user names").add()

                .property().name("emailcol")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("user_email")
                .label("User email Column")
                .helpText("Column name that holds the user emails").add()

                .property().name("passwordcol")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("user_password")
                .label("Password Column")
                .helpText("Column name that holds the user password").add()

                .property().name("keycloak_mysql")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("jdbc:mysql://localhost/my_wiki_test")
                .label("Keycloak MySQL URI")
                .helpText("MySQL URI to connect to Keycloak DB").add()

                .property().name("keycloak_db_username")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("admin")
                .label("Keycloak MySQL DB Username")
                .helpText("MySQL username for Keycloak DB connection").add()

                .property().name("keycloak_db_password")
                .type(ProviderConfigProperty.PASSWORD)
                .defaultValue("password")
                .label("Keycloak MySQL DB Password")
                .helpText("MySQL password for Keycloak DB connection").add()

                .property().name("batchSize")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("1000")
                .label("Batch Size")
                .helpText("Amount of users that will be imported in one batch").add()

                .property().name("lastSyncId")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("0")
                .label("Last synced User id")
                .helpText("Column that holds the last synced MediaWiki User id")
                .add().build();
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return configMetadata;
    }

    public void validateConfiguration(KeycloakSession session, RealmModel realm, ComponentModel config) throws ComponentValidationException {
        if (StringUtil.isBlank(config.get("mysql"))
                || StringUtil.isBlank(config.get("table"))
                || StringUtil.isBlank(config.get("usernamecol"))) {
            throw new ComponentValidationException("Configuration not properly set, please verify.");
        }
    }

    public Map<String, Object> getTypeMetadata() {
        Map<String, Object> metadata = new HashMap();
        if (this instanceof ImportSynchronization) {
            metadata.put("synchronizable", true);
        }

        return metadata;
    }


    @Override
    public SynchronizationResult sync(KeycloakSessionFactory keycloakSessionFactory, String realmId, UserStorageProviderModel userStorageProviderModel) {
        logger.infov("Sync is started");
        SynchronizationResult result;
        List<MediawikiUser> users;
        try (DataSource ds = new DataSource(userStorageProviderModel)) {
            //TODO: Fix config cache problem. Potential case to get old 'lastSyncId' because of config is cached.
            String lastUserId = userStorageProviderModel.getConfig().getFirst("lastSyncId");
            String limit = userStorageProviderModel.getConfig().getFirst("batchSize");
            ds.getConnection();
            users = ds.getUsersBunch(lastUserId, limit);
            result = importMediawikiUsers(keycloakSessionFactory, realmId, userStorageProviderModel, users);
        } catch (SQLException e) {
            logger.error("SQLException: " + e.getMessage());
            throw new RuntimeException(e);
        }
        if (!users.isEmpty()) {
            logger.infov("We got users");
        }

        return result;
    }

    @Override
    public SynchronizationResult syncSince(Date date, KeycloakSessionFactory keycloakSessionFactory, String realmId, UserStorageProviderModel userStorageProviderModel) {
        logger.infov("Sync since is started");
        SynchronizationResult result;

        /* Temp solution to make partial sync button works. It will just start Full sync for now */
        result = sync(keycloakSessionFactory, realmId, userStorageProviderModel);

        return result;
    }

    SynchronizationResult importMediawikiUsers(KeycloakSessionFactory keycloakSessionFactory, final String realmId, final ComponentModel fedModel, List<MediawikiUser> users) {
        final SynchronizationResult syncResult = new SynchronizationResult();
        try {
            KeycloakModelUtils.runJobInTransaction(keycloakSessionFactory, new KeycloakSessionTask() {
                @Override
                public void run(KeycloakSession session) {

                    RealmModel currentRealm = session.realms().getRealm(realmId);
                    session.getContext().setRealm(currentRealm);
                    DBUserStorageProvider storageProvider = (DBUserStorageProvider) keycloakSessionFactory.create().getProvider(UserStorageProvider.class, fedModel);

                    for (MediawikiUser user : users) {
                        UserModel imported = storageProvider.importUserFromMediawiki(session, currentRealm, user);
                        syncResult.increaseAdded();
                    }
                }
            });
        } catch (ModelException me) {
            logger.error("Failed during import user from MediaWiki. Message: {0}", me);
            syncResult.increaseFailed();
        }

        return syncResult;
    }
}
