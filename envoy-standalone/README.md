# Envoy Documentation
This is a proof of concept for an integration between Envoy Proxy and a Keycloak identity server.

The objective is to have a server behind an Envoy proxy which delegates the authentication on the proxy.
The Envoy proxy is responsible of validating the authentication tokens against the Keycloak server, setting the server free of any authentication configuration.

## Configuration description

#### Filter envoy.filters.network.http_connection_manager
Used to navigate through envoy to already pre-define cluster.

#### Filter envoy.filters.http.jwt_authn
Used to verify JSON Web Token (JWT) by provided issuer.

#### Filter envoy.filters.http.router
Implements HTTP forwarding. It will be used in almost all HTTP proxy scenarios that Envoy is deployed for.

#### Clusters
A group of logically similar upstream hosts that Envoy connects to.
