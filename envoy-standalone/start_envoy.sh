#!/bin/bash

WDIR=$(pwd)
CTEMPATE="${WDIR}/envoy_template.yml"
ECONFIG="/home/$(whoami)/envoy.yaml"

envsubst "$(printf '${%s} ' $(env | cut -d'=' -f1))"  < ${CTEMPATE} > ${ECONFIG}

if [ -z $ENVOY_LOG_LEVEL ]; then ENVOY_LOG_LEVEL="info"; fi

envoy -c ${ECONFIG} --log-level $ENVOY_LOG_LEVEL