# Client for envoy-keycloak integration

The simple client that makes an authentified call to the server. The client first gets the authentication token from the identity server, then sends the authenticated request to the server via the Envoy proxy.

# Get Started

### Pre-requisites
- Install [Node.js](https://nodejs.org/en/).

**1.** Setup the dependencies.
```
npm install
```

**2.** Run the client.
```
npm start
```
