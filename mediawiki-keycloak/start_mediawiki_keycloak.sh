#!/bin/bash

INSTALLATION_SCRIPT="/var/www/html/maintenance/install.php"
LOCAL_SETTINGS="/var/www/html/LocalSettings.php"
CUSTOM_SETTINGS="/var/www/html/CustomSettings.php"

if [ -z $WM_INSTANCE ]; then
    echo "Mandatory WM_INSTANCE env var is not set. We need this variable to take an appropriate CA config. Aborting."
    exit 1
fi

CUSTOM_SETTINGS_TEMPLATE="/root/${WM_INSTANCE}_conf.tmpl"

php ${INSTALLATION_SCRIPT} \
    --dbname="${DBNAME}" \
    --dbserver="${DBSERVER}" \
    --dbuser="${DBUSER}" \
    --dbpass="${DBPASSWORD}" \
    --server="${SERVERNAME}" \
    --scriptpath="" \
    --lang="${LANGUAGE}" \
    --pass="${ADMIN_PASSWORD}" \
    "${MEDIAWIKI_NAME}" "Admin"

# create tables for centralauth
php maintenance/sql.php --wikidb centralauth extensions/CentralAuth/schema/mysql/tables-generated.sql

PROTOHOST1=$(echo ${HOSTNAMES_COMBINED} | cut -d ';' -f 1)
PROTOHOST2=$(echo ${HOSTNAMES_COMBINED} | cut -d ';' -f 2)
PROTOHOST3=$(echo ${HOSTNAMES_COMBINED} | cut -d ';' -f 3)

HOST1=$(echo ${PROTOHOST1} | cut -d ':' -f 2,3)
HOST2=$(echo ${PROTOHOST2} | cut -d ':' -f 2,3)
HOST3=$(echo ${PROTOHOST2} | cut -d ':' -f 2,3)

# central auth - fix hostnames
sed -e "s#__DBSERVER__#${DBSERVER}#" \
        -e "s#__DBSERVER__#${DBSERVER}#" \
        -e "s#__DBSERVER__#${DBSERVER}#" \
        -e "s#__HOST1_WITH_SLASHES__#${HOST1}#" \
        -e "s#__HOST2_WITH_SLASHES__#${HOST2}#" \
        -e "s#__HOST3_WITH_SLASHES__#${HOST3}#" \
        -e "s#__PROTOHOST1__#${PROTOHOST1}#" \
        -e "s#__PROTOHOST2__#${PROTOHOST2}#" \
        -e "s#__PROTOHOST3__#${PROTOHOST3}#" ${CUSTOM_SETTINGS_TEMPLATE} > ${CUSTOM_SETTINGS}

#echo "ServerName ${SERVERNAME#htttp://}" >> /etc/apache2/apache2.conf
sed -i 's#$wgScriptPath = "/";#$wgScriptPath = "";#' ${LOCAL_SETTINGS}

envsubst "$(printf '${%s} ' $(env | cut -d'=' -f1))"  < ${CUSTOM_SETTINGS_TEMPLATE} > ${CUSTOM_SETTINGS}
grep -q "@include('${CUSTOM_SETTINGS}');" ${LOCAL_SETTINGS} || echo "@include('${CUSTOM_SETTINGS}');" >> ${LOCAL_SETTINGS}

apache2-foreground
