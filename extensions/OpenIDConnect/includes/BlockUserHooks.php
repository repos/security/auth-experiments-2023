<?php

namespace MediaWiki\Extension\OpenIDConnect;

use Config;
use Jumbojett\OpenIDConnectClient;
use MediaWiki\Hook\BeforePageDisplayHook;
use OutputPage;
use Skin;

class BlockUserHooks implements BeforePageDisplayHook {

	private Config $mainConfig;

	/**
	 * @param Config $mainConfig
	 */
	public function __construct( Config $mainConfig ) {
		$this->mainConfig = $mainConfig;
	}

	/**
	 * @param OutputPage $out
	 * @param Skin $skin
	 * @todo Add 1x1 images somewhere besides page content
	 */
	public function onBeforePageDisplay( $out, $skin ): void {
		$user = $out->getContext()->getUser();
		$session = $out->getRequest()->getSession();
		$at = $session->get('access_token');
		$rt = $session->get('refresh_token');
		$exp = $session->get('exp');
		if ( $exp != null && $exp <= time() ) {
			$config = $this->mainConfig->get('PluggableAuth_Config');

			$oidc = new OpenIDConnectClient(
				$config[0]['data']['providerURL'],
				$config[0]['data']['clientID'],
				$config[0]['data']['clientsecret']
			);

			$response = $oidc->refreshToken($out->getRequest()->getSessionData('refresh_token'));
			if (isset($response->error)) {
				$out->addWikiMsg($response->error);
//				$this->pluggableAuthService->deauthenticate($user->getName());
				$user->logout();
			} elseif (isset($response->expires_in)) {
				$out->addWikiMsg($response->access_token);
				$out->addWikiMsg($response->refresh_token);
				$session->set('exp', time()+$response->expires_in);
			} else {
				$out->addWikiMsg('Last Condition. Response: ', $response);
			}
		}
	}
}
