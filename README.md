# Mediawiki-Common

Common repository for deploying mediawiki instances

## Getting started
1. Get [Docker](https://docs.docker.com/get-docker/).
2. Clone this repo to your projects dir and go into folder.
3. Run `docker-compose up -d` to start Mediawiki instances.

###### Instances will be available by URLs:
- http://localhost:8080 - Mediawiki_1  instance with CentralAuth configuration.
- http://localhost:8081 - Mediawiki_2 instance with CentralAuth configuration.
- http://localhost:8082 - Mediawiki instance with Keycloak (PluggableAuth)configuration & CentralAuth configuration.
- http://localhost:9000 - Keycloak Interface.
- http://localhost:10000 - Envoy Instance.

###### Verification

Once docker-compose scenario, for local development, is up - you can perform a verification using such commands:
```
export CLIENT_ID="poc-client"
export CLIENT_SECRET="adminQWERTY123!"

# this command will generate a JWT token for the poc-client id
JWT_TOKEN=$(sudo docker exec \
    -i mediawiki-common_mediawiki1_1 \
    curl -s -XPOST \
    --data client_id=${CLIENT_ID} \
    --data client_secret=${CLIENT_SECRET} \
    --data grant_type=client_credentials \
    http://keycloak:8080/realms/Envoy-Keycloak-POC/protocol/openid-connect/token \
    | jq -r .access_token \
    )

# show the token in b64
echo $JWT_TOKEN
...

# now we can parse the token, to get it's details
jq -R 'split(".") | .[0],.[1] | @base64d | fromjson' <<< $(echo "${JWT_TOKEN}")
{
  "alg": "RS256",
  "typ": "JWT",
  "kid": "7Le8V1R0Xhllz2R5Q0lltjdAVojr-wR_Tp2OKjGnN8U"
}
{
  "exp": 1668869239,
  "iat": 1668868939,
  "jti": "7d66cf85-4a70-4be0-86eb-1cb546b1f235",
  "iss": "http://keycloak:8080/realms/Envoy-Keycloak-POC",
  "sub": "fb4ba45f-503a-44e4-8548-e2293505fb1c",
  "typ": "Bearer",
  "azp": "poc-client",
  "scope": "email profile",
  "clientId": "poc-client",
  "clientHost": "172.31.0.4",
  "email_verified": false,
  "preferred_username": "service-account-poc-client",
  "clientAddress": "172.31.0.4"
}

# now we can try to reachout to the wikimedia resource.
# we will get the target page code after a successful validation of the JWT token against the keycloak issuer
$ curl -XGET -H "Authorization: Bearer ${JWT_TOKEN};" http://localhost:10000/api.php?action=parse&format=json&page=Demo_article&prop=text&formatversion=2
...
```

## Configure local environment

1. Open [Keycloak](http://localhost:9000) and log in to **Administration Console**. Admin credential is configured by `docker-compose.yml` file (see **keycloak** container environment).
2. In the **Login** category of the **Realm Settings** section fill in the required fields and save:
  - *User registration* -> `ON`
  - *Edit username* -> `ON`
3. In the **Clients** section of the **Envoy-Keycloak-POC** realm create a new client with the `openid-connect` protocol (As an example named Client ID `local-oidc`).
4. In the **Settings** category of the **local-oidc** client fill in the required fields and save:
  - *Access Type* -> `confidential`
  - *Authorization Enabled* -> `ON`
  - *Service Accounts Enabled* -> `ON`
  - *Root URL* -> `http://localhost:8082/index.php`
  - *Valid Redirect URIs* -> `/Special:PluggableAuthLogin`
5. In the **local-oidc** client required credentials are:
  - Copy **Client ID** value of the **Settings** category to `docker-compose.yml` -> `mediawiki1_keycloak` -> `CS_PROVIDER_ID`.
  - Copy **Secret** value of the **Credentials** category to `docker-compose.yml` -> `mediawiki1_keycloak` -> `CS_CLIENT_SECRET`.
6. Fill in the variable `CS_PROVIDER_URL` using the template `http://keycloak-ip-address:8080/realms/realm-name` (Important [Specific issue with IP address access](https://gitlab.gluzdov.com/auth-experiment/mediawiki-common#specific-issue-with-ip-address-access) topic). To  get `realm-name` go to **Realm Settings** section and copy the value of **Name** field. Follow the instruction on how to get `keycloak-ip-address`:
```
# run this command to get the list of containers with ids
docker ps --format '{{.ID}} {{.Image}}'
# run this command with parameter id of the mediawiki-common-keycloak container from the list above
docker inspect [id] | grep IPAddress
```
7. OIDC configuration is done. Rebuild the `mediawiki1_keycloak` container to apply changes:
```
docker-compose up -d --no-deps --build mediawiki1_keycloak
```
8. In the **User Federation** section create a new provider `mediawiki-user-provider`. Fill in the required settings and save:
  - *MySQL URI* -> `jdbc:mysql://mysql-ip-address:3306/centralauth` (use same instruction as for getting `keycloak-ip-address`)
  - *MySQL DB Username* -> `root`
  - *MySQL DB Password* -> Password is configured by `docker-compose.yml` file (see **database** container environment)
  - *Users Table* -> `globaluser`
  - *Username Column* -> `gu_name`
  - *User real name Column* -> `gu_name`
  - *User email Column* -> `gu_email`
  - *Password Column* -> `gu_password`
  - *Keycloak MySQL URI* -> `jdbc:mysql://mysql-ip-address:3306/keycloak` (use `mysql-ip-address` from *MySQL URI*)
  - *Keycloak MySQL DB Username* -> `root`
  - *Keycloak MySQL DB Password* -> Use *MySQL DB Password* value
  - *Cache Policy* -> `NO_CACHE`

  Fields *Batch Size*, *Last synced User id*, *Periodic Full Sync*, *Periodic Changed Users Sync* are optional, you can get more details [here](https://gitlab.gluzdov.com/auth-experiment/mediawiki-common/-/blob/main/keycloak-standalone/mediawiki-user-federation/README.md).

**Now the environment is ready for local development!**

## Specific issue with IP address access
Within docker network, all containers can directly reach each other via domains, IP addresses, and ports on which the service is running.
But when containers are internally redirected to each other, it causes issue. In this case, containers’ host doesn't know anything about the other containers’ names. To fix this use the direct IP address to the container for the configuration of the `docker-compose.yml` -> `mediawiki1_keycloak` -> `CS_PROVIDER_URL` environment. Please keep in mind different operation systems' specific:
  - Docker for Mac does not expose container networks directly on the macOS host. Docker for Mac works by running a Linux VM under the hood (using hyperkit) and creating containers within that VM. The solution is to create a minimal network tunnel between macOS and the Docker Desktop Linux VM using [Docker Mac Net Connect](https://github.com/chipmk/docker-mac-net-connect).
  - Docker for Linux [How to Get Docker Container's IP Address](https://linuxhandbook.com/get-container-ip/).
 
